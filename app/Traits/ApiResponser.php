<?php
namespace App\Traits;

use App\Models\Interfaces\HttpStatusResponser;
use Illuminate\Support\Collection;;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

trait ApiResponser
{
    private function successResponse($responseParams, $statusCode = HttpStatusResponser::OK)
    {
        return response()->json($responseParams, $statusCode);
    }

    protected function errorResponse($message, $statusCode)
    {
        return response()->json(['error' => $message, 'code' => $statusCode], $statusCode);
    }

    protected function showAll(Collection $collection, $statusCode = HttpStatusResponser::OK)
    {
        if($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $statusCode);
        }

        $transformer = $collection->first()->transformer;
        $collection = $this->filterData($collection, $transformer);
        $collection = $this->sort($collection, $transformer);
        $total = $collection->count();
        $collection = $this->paginate($collection);
        $collection = $this->transformData($collection, $transformer);
        $collection = $this->cacheResponse($collection);
        $responseParams = ['data' => $collection, 'count' => $total];
        return $this->successResponse($responseParams, $statusCode);
    }

    protected function showOne(Model $model, $statusCode = HttpStatusResponser::OK)
    {
        $transformer = $model->transformer;
        $model = $this->transformData($model, $transformer);
        $collection = $this->cacheResponse($model);
        $responseParams = ['data' => $model];
        return $this->successResponse($responseParams, $statusCode);
    }

    protected function showMessage(string $message, int $statusCode = HttpStatusResponser::OK)
    {
        $responseParams = ['data' => $message];
        return $this->successResponse($responseParams, $statusCode);
    }

    private function cacheResponse(mixed $data)
    {
        $url = request()->url();
        $queryParams = request()->query();
        ksort($queryParams);
        $queryString = http_build_query($queryParams);
        $fullUrl = "{$url}?{$queryString}";

        return Cache::remember($fullUrl, 30, function () use($data) {
            return $data;
        });
    }

    private function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:10|max:100'
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();
        $elementsPerPage = 15; // Default Page Size

        // Update elements per page if there is querystring present!
        if(request()->has('per_page')) {
            $elementsPerPage = (int)request()->per_page;
        }

        $results = $collection->slice($elementsPerPage*($page-1), $elementsPerPage)->values();

        $options = [
            'page' => LengthAwarePaginator::resolveCurrentPath()
        ];
        $paginator = new LengthAwarePaginator($results, $collection->count(), $elementsPerPage, $page, $options);
        $paginator->appends(request()->all());
        return $paginator;
    }

    private function filterData(Collection $collection, $transformer): Collection
    {
        foreach(request()->query() as $key => $value) {
            $actualAttribute = $transformer::attributeMapper($key);
            if(isset($actualAttribute, $value)) {
                $collection = $collection->where($actualAttribute, $value);
            }
        }

        return $collection;
    }

    private function sort(Collection $collection, $transformer): Collection
    {
        if(request()->has('sort_by')) {
            $transformedAttribute = request()->sort_by;
            $sortAttribute = $transformer::attributeMapper($transformedAttribute);
            if(request()->has('order_by') && request()->order_by === 'desc') {
                $collection = $collection->sortByDesc($sortAttribute);
            } else {
                $collection = $collection->sortBy($sortAttribute);
            }
        }
        return $collection;
    }


    # Note: 'data' key is passed automatically by transformer.
    private function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);
        return collect($transformation->toArray()['data']);
    }
}
