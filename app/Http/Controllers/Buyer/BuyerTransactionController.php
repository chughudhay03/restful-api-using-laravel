<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BuyerTransactionController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only('index');
    }
    
    public function index(Buyer $buyer): JsonResponse
    {
        $transactions = $buyer->transactions;
        return $this->showAll($transactions);
    }
}
