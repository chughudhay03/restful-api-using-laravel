<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index', 'show']);
<<<<<<< HEAD
        $this->middleware('auth:api')->except(['index','show']);
        $this->middleware('transform.input:' . ProductTransformer::class)->only(['store', 'update']);
=======
>>>>>>> feature/auth
    }

    public function index(): JsonResponse
    {
        $products = Product::all();
        return $this->showAll($products);
    }

    public function show(Product $product): JsonResponse
    {
        return $this->showOne($product);
    }
    
}
