<?php

namespace App\Http\Controllers;

use App\Models\Interfaces\HttpStatusResponser;
use App\Traits\ApiResponser;

class ApiController extends Controller implements HttpStatusResponser
{
    use ApiResponser;
}
