<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\Mail\UserCreated;
use App\Models\Interfaces\HttpStatusResponser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UsersController extends ApiController
{
    public function __construct()
    {
        $this->middleware('transform.input' . UserTransformer::class)->only(['store', 'update']);
        $this->middleware('client.credentials')->only(['index', 'store', 'show', 'resend']);
        $this->middleware('auth:api')->except(['index', 'store', 'show', 'resend']);
    }

    public function index()
    {
        $users = User::all();
        return $this->showAll($users);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerficationCode();
        $data['admin'] = User::REGULAR_USER;

        $user = User::create($data);

        return $this->showOne($user, HttpStatusResponser::CREATED);
    }

    public function show(User $user)
    {
        return $this->showOne($user);
    }
    public function update(Request $request, User $user)
    {
        $rules = [
            'email' => 'email|unique:users,email,'.$user->id,
            'password' => 'min:8|confirmed',
            'admin' => 'in:'. User::REGULAR_USER . ',' . User::ADMIN_USER,
        ];

        $this->validate($request, $rules);

        if($request->has('name')) {
            $user->name = $request->name;
        }

        if($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if($request->has('email')) {
            if($request->email != $user->email) {
                $user->email = $request->email;
                $user->verified = User::UNVERIFIED_USER;
                $user->verification_token = User::generateVerficationCode();
                $user->admin = User::REGULAR_USER;
            }
        }

        if($request->has('admin')) {
            if(!$user->isVerified()) {
                return $this->errorResponse('Only verified users can modify the admin fields', HttpStatusResponser::CONFLICT);
            }
        }

        if(!$user->isDirty()) {
            return $this->errorResponse('You need to update some data!', HttpStatusResponser::UNPROCESSABLE_ENTITY);
        }

        $user->save();

        return $this->showOne($user);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return $this->showOne($user, HttpStatusResponser::NO_CONTENT);
    }

    public function verify($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = User::VERIFIED_USER;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage("The account has been verified!");
    }

    public function resend(User $user)
    {
        if($user->isVerified()) {
            return $this->errorResponse('You are already verified', self::CONFLICT);
        }

        retry(5, function () use($user) {
            Mail::to($user)->send(new UserCreated($user));
        }, 250);

        return $this->showMessage('Email Sent!');
    }
}
