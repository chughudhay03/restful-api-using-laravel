<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\ApiController;
use App\Models\Category;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoriesController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index', 'show']);
        $this->middleware('auth:api')->except(['index','show']);
        $this->middleware('transform.input:' . CategoryTransformer::class)->only(['store', 'update']);
    }

    public function index(): JsonResponse
    {
        $categories = Category::all();
        return $this->showAll($categories);
    }

    public function show(Category $category): JsonResponse
    {
        return $this->showOne($category);
    }

    public function store(Request $request): JsonResponse
    {
        $rules = [
            'name' => 'required|min:2|max:255',
            'description' => 'required|min:2'
        ];
        $this->validate($request, $rules);

        $category = Category::create($request->only(['name', 'description']));
        return $this->showOne($category, self::CREATED);
    }

    public function update(Request $request, Category $category): JsonResponse
    {
        $category->fill($request->only(['name', 'description']));
        if($category->isClean()) {
            return $this->errorResponse('You need to update some field!', self::UNPROCESSABLE_ENTITY);
        }

        $category->save();
        return $this->showOne($category);
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category, self::NO_CONTENT);
    }
}
