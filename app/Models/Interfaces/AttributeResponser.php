<?php
namespace App\Models\Interfaces;

interface AttributeResponser {
    public static function attributeMapper(string $key): ?string;
    public static function getTransformedAttribute(string $key): ?string;
}
