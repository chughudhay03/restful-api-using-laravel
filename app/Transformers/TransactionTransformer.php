<?php

namespace App\Transformers;

use App\Models\Interfaces\AttributeResponser;
use App\Models\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract implements AttributeResponser
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'identifier' => (int)$transaction->id,
            'transacBuyer' => (int)$transaction->buyer_id,
            'transacProduct' => (int)$transaction->product_id,
            'quantity' => (string)$transaction->quantity,
            'creationDate' => $transaction->created_at,
            'lastChange' => $transaction->updated_at,
            'deletedDate' => isset($transaction->deleted_at) ? (string)$transaction->deleted_at : null,
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('transactions.show', $transaction->id)
                ],
                [
                    'rel' => 'transaction.categories',
                    'href' => route('transactions.categories.index', $transaction->id)
                ],
                [
                    'rel' => 'transaction.sellers',
                    'href' => route('transactions.sellers.index', $transaction->id)
                ],
            ]
        ];
    }

    public static function attributeMapper(string $key): ?string
    {
        $attributes = [
            'identifier' => 'id',
            'transacBuyer' => 'buyer_id',
            'transacProduct' => 'product_id',
            'quantity' => 'quantity',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deletedDate' => 'deleted_at',
        ];

        return $attributes[$key] ?? null;
    }

    public static function getTransformedAttribute(string $key): ?string
    {
        $attributes = [
            'id' => 'identifier',
            'buyer_id' => 'transacBuyer',
            'product_id' => 'transacProduct',
            'quantity' => 'quantity',
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange',
            'deleted_at' => 'deletedDate',
        ];

        return $attributes[$key] ?? null;
    }

}
